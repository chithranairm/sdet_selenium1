package com.wipro.SDET_Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Olay {

WebDriver driver;
	
	@BeforeTest
	public void setup()
	{
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.olay.com/login.php");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	}
	
	@AfterTest
	public void teardown()
	{
		
		driver.quit();
		
	}
	
	// navigate to Sign up Page
    @Test(enabled = false)
    public void verifySignupnavigation()
    {
    	driver.manage().deleteAllCookies();
    	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        
        WebElement loginLink = driver.findElement(By.xpath("//*[@id=\"custom_page_container\"]/header/nav/ul/li[4]/a/img"));
        loginLink.click(); 
        WebElement createaccountlink=driver.findElement(By.xpath("//*[@id=\"loginModal\"]/div[1]/div/div/form/div[4]/a"));
        createaccountlink.click();
        String actualTitle =driver.getTitle();
    	String expectedTitle ="CREATE AN ACCOUNT";
    	if(actualTitle.equalsIgnoreCase(expectedTitle))
			System.out.println("Suceesfully navigated to sign up page");
		else
			System.out.println("Sign up page navigation is failed");
    	
        
    }
    
 // Verify Registration Page fields
    @Test(enabled = false)
    public void verifySignuppagefields()
    {
    	driver.manage().deleteAllCookies();
    	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        
        //navigation to sign up page
        WebElement loginLink = driver.findElement(By.xpath("//*[@id=\"custom_page_container\"]/header/nav/ul/li[4]/a/img"));
        loginLink.click(); 
        WebElement createaccountlink=driver.findElement(By.xpath("//*[@id=\"loginModal\"]/div[1]/div/div/form/div[4]/a"));
        createaccountlink.click();
        
        //sign up page fields
        WebElement firstname = driver.findElement(By.xpath("//*[@id=\"FormField_4_input\"]"));
        WebElement lastname = driver.findElement(By.xpath("//*[@id=\"FormField_5_input\"]"));
        WebElement zip = driver.findElement(By.xpath("//*[@id=\"FormField_13_input\"]"));
        WebElement email = driver.findElement(By.xpath("//*[@id=\"FormField_1_input\"]"));
        WebElement password = driver.findElement(By.xpath("//*[@id=\"FormField_2_input\"]"));
        WebElement confirmpassword = driver.findElement(By.xpath("//*[@id=\"FormField_3_input\"]"));
        
        if(firstname.isDisplayed()&&lastname.isDisplayed()&&firstname.isDisplayed()&&firstname.isDisplayed()&&password.isDisplayed()&&confirmpassword.isDisplayed()
        		&&zip.isDisplayed()&&email.isDisplayed())
        System.out.println("The required fields are present in sign up page");
        else
        	System.out.println("The required fields are not present in sign up page");	   
        
    }
    
    
 // Registration with all valid data
    @Test(enabled = false)
    public void validRegistrationTest(){  
    	
    	driver.manage().deleteAllCookies();
    	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        
        //navigation to sign up page
        WebElement loginLink = driver.findElement(By.xpath("//*[@id=\"custom_page_container\"]/header/nav/ul/li[4]/a/img"));
        loginLink.click(); 
        WebElement createaccountlink=driver.findElement(By.xpath("//*[@id=\"loginModal\"]/div[1]/div/div/form/div[4]/a"));
        createaccountlink.click();
        
              
    	WebElement firstname = driver.findElement(By.xpath("//*[@id=\"FormField_4_input\"]"));
    	firstname.sendKeys("Test");
             
    	WebElement lastname = driver.findElement(By.xpath("//*[@id=\"FormField_5_input\"]"));
    	lastname.sendKeys("abc");
    	
    	WebElement zip = driver.findElement(By.xpath("//*[@id=\"FormField_13_input\"]"));
    	zip.sendKeys("32003");
            
             
        WebElement email = driver.findElement(By.xpath("//*[@id=\"FormField_1_input\"]"));
        email.sendKeys("test123456789@gmail.com");
         
        WebElement password = driver.findElement(By.xpath("//*[@id=\"FormField_2_input\"]"));
        password.sendKeys("Test@12345");
        
        WebElement confirmpassword = driver.findElement(By.xpath("//*[@id=\"FormField_3_input\"]"));
        confirmpassword.sendKeys("Test@12345");
 
        
        Select bmonth=new Select(driver.findElement(By.xpath("//*[@id=\"FormField_25_month\"]")));
        bmonth.selectByValue("4");
		  
		  Select byear=new Select(driver.findElement(By.xpath("//*[@id=\"FormField_25_year\"]")));
		  byear.selectByValue("2000"); 
		  
		  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS); 
        //driver.findElement(By.xpath("//*[@id=\"recaptcha-anchor\"]/div[2]")).click();
        
        
        WebElement joinbutton = driver.findElement(By.linkText("JOIN"));
        joinbutton.click();
        
       //Sign up validation
		  String expectedURL = "https://www.olay.com/manage-account"; 
		  String actualURL = driver.getCurrentUrl(); 
		  Assert.assertEquals(actualURL,expectedURL);
		  
		  String expectedTitle = "Test, we're glad you're here!"; 
		  String actualTitle = driver.getTitle(); 
		  Assert.assertEquals(actualTitle,expectedTitle);
		     
    }
    
     
    @Test(dataProvider="Login",enabled = false)
    public void verifyLogin(String username, String password){  
    	
    	driver.manage().deleteAllCookies();
    	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        
  		
  		//Login credentials
  		driver.findElement(By.name("login_email")).sendKeys(username);
  		driver.findElement(By.name("login_pass")).sendKeys(password);
  		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
  		
  		
  		//Login Button
  		driver.findElement(By.xpath("//*[@id=\"main-content\"]/div/div[3]/div/form/div[3]/input")).click();
  		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
  	
          
  		//Logout button
  		driver.findElement(By.xpath("//*[@id=\"custom_page_container\"]/header/nav/ul/li[4]/a/img")).click();
  		driver.findElement(By.xpath("//*[@id=\"login-dropdown-menu\"]/li[2]/a")).click();
  		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
  		
    }
    
    
    @Test(dataProvider="invalidLogin",enabled=true)
    public void verifyinvalidLogin(String username, String password){  
    	
    	driver.manage().deleteAllCookies();
    	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        
  		
  		//Login credentials
  		driver.findElement(By.name("login_email")).sendKeys(username);
  		driver.findElement(By.name("login_pass")).sendKeys(password);
  		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
  		
  		
  		//Login Button
  		driver.findElement(By.xpath("//*[@id=\"main-content\"]/div/div[3]/div/form/div[3]/input")).click();
  		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
  		
  	
          String expectederror="Invalid credentials. Please try again";
          String actualerror = driver.findElement(By.xpath("//*[@id=\"main-content\"]/div/div[3]/div/div")).getText();
  		
          Assert.assertEquals(actualerror,expectederror);
  		
    }
    
    
    @DataProvider(name="Login")
	public Object[][] getData()
	{
		
		Object[][] data= new Object[3][2];
		
		data[0][0]="test123456789@gmail.com";
		data[0][1]="Test@12345";
		
		
		return data;
	}
	
    @DataProvider(name="invalidLogin")
	public Object[][] getInvalidData()
	{
		
		Object[][] data= new Object[3][2];
		
		data[0][0]="test123456789@gmail.com";
		data[0][1]="Test";
		
		
		return data;
	}
}
