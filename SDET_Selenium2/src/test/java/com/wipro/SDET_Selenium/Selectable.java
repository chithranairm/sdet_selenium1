package com.wipro.SDET_Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Selectable {

WebDriver driver;
	
	@BeforeTest
	public void setup()
	{
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	}
	
	@AfterTest
	public void teardown()
	{
		
		driver.quit();
		
	}
	
	@Test
	public void testcase1()
	{
		driver.get("https://demoqa.com/selectable/");
        driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[1]")).click();
		String s1=driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[1]")).getText();
		System.out.println(s1);
		driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[2]")).click();
		String s2=driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[2]")).getText();
		System.out.println(s2);
		driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[3]")).click();
		String s3=driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[3]")).getText();
		System.out.println(s3);
		driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[4]")).click();
		String s4=driver.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[4]")).getText();
		System.out.println(s4);

		
	}
	
	
	
	
}
