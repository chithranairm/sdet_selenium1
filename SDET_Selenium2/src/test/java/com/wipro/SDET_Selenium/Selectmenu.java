package com.wipro.SDET_Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Selectmenu {

WebDriver driver;


	
	@BeforeTest
	public void setup()
	{
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/select-menu/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	}
	
	@AfterTest
	public void teardown()
	{
		
		driver.quit();
		
	}
	
	@Test(priority=1)
	public void verifySelectmenu()
	{
	
		WebElement sel3=driver.findElement(By.id("oldSelectMenu"));
		if(sel3.isDisplayed())
			System.out.println("Drop down menu is present ");
		else
			System.out.println("Drop down menus is not present");
        
	
		
	}
	@Test(priority=2)
	public void dropdownSelection()
	{
	
	
		  Select menu1=new Select(driver.findElement(By.xpath("//*[@id=\"oldSelectMenu\"]")));
		  menu1.selectByValue("red");
		  menu1.selectByIndex(1);
		  menu1.selectByVisibleText("Yellow");
		 
		
	}
	
	
}
