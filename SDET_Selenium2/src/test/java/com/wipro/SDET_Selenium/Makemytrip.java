package com.wipro.SDET_Selenium;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Makemytrip {

WebDriver driver;
	
	@BeforeTest
	public void setup()
	{
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.makemytrip.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	}
	
	@AfterTest
	public void teardown()
	{
		
		driver.quit();
		
	}
	
	
    @Test(enabled = true)
    public void VerifyBooking()
    {
    	
    	String actualTitle=driver.getTitle();
        String url=driver.getCurrentUrl();
        System.out.println(url);
        String expectedTitle=actualTitle;
        if(actualTitle.contentEquals(expectedTitle)){
            System.out.println("Navigated to Makemytrip website");
        }
        else{
            System.out.println("URL Navigation failed");
        }
		
		WebElement roundtrip=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div[1]/ul/li[3]"));
		  roundtrip.click();
		 
    	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        
    }
	
	
}
