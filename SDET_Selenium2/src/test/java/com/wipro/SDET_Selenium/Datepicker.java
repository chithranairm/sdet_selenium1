package com.wipro.SDET_Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Datepicker {

WebDriver driver;
	
	@BeforeTest
	public void setup()
	{
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/date-picker/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	}
	
	@AfterTest
	public void teardown()
	{
		
		driver.quit();
		
	}
	
	@Test(priority=1)
	public void verifyDatefield()
	{
		WebElement datebox=driver.findElement(By.id("datePickerMonthYearInput"));
		WebElement datetimebox=driver.findElement(By.id("dateAndTimePickerInput"));
		
		if(datebox.isDisplayed()&&datetimebox.isEnabled()&&datetimebox.isDisplayed()&&datetimebox.isEnabled())
			System.out.println("Date field is available ");
		else
			System.out.println("Date field is not available");
        
		
		
	}
	
	
	
	@Test(dataProvider = "inputDates",priority=2)
	public void dateTimePicker(String ipDate, String ipDateTime) throws NullPointerException
	{
		WebElement datebox=driver.findElement(By.id("datePickerMonthYearInput"));
		datebox.click();
		datebox.clear();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		datebox.sendKeys(ipDate);
		String out1=datebox.getAttribute("value");
		System.out.println("Selected date is"+out1);
		
		WebElement datetimebox=driver.findElement(By.id("dateAndTimePickerInput"));
		datetimebox.click();
		datetimebox.clear();
		
		datetimebox.sendKeys(ipDateTime);
		String out2=datetimebox.getAttribute("value");
		System.out.println("Selected time is"+out2);
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
       
   
		
	}
	
	
	@DataProvider(name = "inputDates")
    public static Object[][] getDates() {
Object[][] date= new Object[3][2];
		
		date[0][0]="08/24/2021";
		date[0][1]="August 24, 2021 12:12 PM";
		
	
		return date;
	}   
}
