package com.wipro.SDET_Selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Contract_Form {

WebDriver driver;
	
	@BeforeTest
	public void setup()
	{
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/automation-practice-form/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	}
	
	@AfterTest
	public void teardown()
	{
		
		driver.quit();
		
	}
	
	@Test(priority=1)
	public void verifyFirstname()
	{
		
		WebElement firstname= driver.findElement(By.xpath("//*[@id=\"firstName\"]"));
		
		if(firstname.isDisplayed()&&firstname.isEnabled())
			System.out.println("FirstName field is available in the registration form");
		else
			System.out.println("FirstName field is not available in the registration form");
        
	}
	
	@Test(priority=1)
	public void verifyLastname()
	{
		
		WebElement lastname= driver.findElement(By.xpath("//*[@id=\"lastName\"]"));
		
		if(lastname.isDisplayed()&&lastname.isEnabled())
			System.out.println("LastName field is available in the registration form");
		else
			System.out.println("LastName field is not available in the registration form");
        
	}
	
	@Test(priority=1)
	public void verifyEmail()
	{
		
		WebElement email= driver.findElement(By.xpath("//*[@id=\"userEmail\"]"));
		
		if(email.isDisplayed()&&email.isEnabled())
			System.out.println("Email field is available in the registration form");
		else
			System.out.println("Email field is not available in the registration form");
        
	}
	
	@Test(priority=1)
	public void verifySubject()
	{
		
		WebElement subject= driver.findElement(By.xpath("//*[@id=\"subjectsContainer\"]"));
		
		if(subject.isDisplayed()&&subject.isEnabled())
			System.out.println("Subject field is available in the registration form");
		else
			System.out.println("Subject field is not available in the registration form");
        
	}
	
	@Test(priority=1)
	public void verifySubmitbutton()
	{
		
		WebElement submit= driver.findElement(By.id("submit"));
		
		if(submit.isDisplayed()&&submit.isEnabled())
			System.out.println("Subject field is available in the registration form");
		else
			System.out.println("Subject field is not available in the registration form");
        
	}
	
	
	
}
